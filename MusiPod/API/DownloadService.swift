//
//  DownloadService.swift
//  MusiPod
//
//  Created by Dai Long on 7/13/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

protocol DownloadServiceDelegate {
    func trackAlreadyDownloaded()
}

class DownloadService {
    
    // MARK: - Properties
    
    var activedDownload: [ URL : Download] = [:]
    
    // MARK: - SearchVC crete downloadSession
    
    var downloadSession: URLSession!
    
    var delegate: DownloadServiceDelegate?
    
    // MARK: - Methods
    
    func startDownload(_ track: Track) {
        
        let download = Download(track)

        download.task = downloadSession.downloadTask(with: track.UrlJunDownload)
        download.task?.resume()
        
        if let sourceURL = download.task?.currentRequest?.url {
            print(sourceURL)
            let destinationURL = save2doc(for: sourceURL)
            
            let fileManager = FileManager.default
            
            if fileManager.fileExists(atPath: destinationURL.path) {
                print("File exist, not save")
                delegate?.trackAlreadyDownloaded()
                download.task?.cancel()
                return
            }
        }
        download.isDownloading = true
        activedDownload[download.track.UrlJunDownload] = download
    }
    
    func save2doc(for url: URL) -> URL {
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentURL.appendingPathComponent(url.lastPathComponent)
    }
    
    func pauseDownload(_ track: Track) {
        guard let downloadModel = activedDownload[track.UrlJunDownload] else {return}
        if downloadModel.isDownloading {
            downloadModel.task?.cancel(byProducingResumeData: { (data) in
                downloadModel.resumeData = data
            })
            downloadModel.isDownloading = false
        }
    }
    
    func resumeDownload(_ track: Track) {
        guard let downloadModel = activedDownload[track.UrlJunDownload] else {return}
        if let data = downloadModel.resumeData {
            downloadModel.task = downloadSession.downloadTask(withResumeData: data)
        }
        else {
            downloadModel.task = downloadSession.downloadTask(with: track.UrlJunDownload)
        }
        
        downloadModel.task?.resume()
        downloadModel.isDownloading = true
    }
    
    func cancelDownload(_ track: Track) {
        if let downloadModel = activedDownload[track.UrlJunDownload] {
            downloadModel.task?.cancel()
            activedDownload[track.UrlJunDownload] = nil
        }
    }
}
