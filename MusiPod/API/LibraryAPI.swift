//
//  LibraryAPI.swift
//  MusiPod
//
//  Created by Dai Long on 7/4/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

//singleton
final class LibraryAPI {
    static var shared = LibraryAPI()
    
    var searchResult: [Track] = []
    private init() {
//        NotificationCenter.default.addObserver(self, selector: #selector(downloadImage(with:)), name: .BLDownloadImage, object: nil)
    }
    
    private let persistencyManager = PersistencyManager()
   
 //   private let httpClient = HTTPClient()
  //  private let isOnline = false
    
  
    
    func getTracks() -> [Track] {
        return persistencyManager.getTracks()
    }
    
//    func addAlbum(_ album: Album, at index: Int) {
//        persistencyManager.addAlbum(album, at: index)
//        if isOnline {
//            httpClient.postRequest("/api/addAlbum", body: album.description)
//        }
//    }
    
//    func deleteAlbum(at index: Int) {
//        persistencyManager.deleteAlbum(at: index)
//        if isOnline {
//            httpClient.postRequest("/api/deleteAlbum", body: "\(index)")
//        }
//    }
//
//    @objc func downloadImage(with notification: Notification) {
//        guard let userInfo = notification.userInfo,
//            let imageView = userInfo["imageView"] as? UIImageView,
//            let coverUrl = userInfo["coverUrl"] as? String,
//            let filename = URL(string: coverUrl)?.lastPathComponent else {return}
//
//        //Retrieve the image from the PersistencyManager if it's been downloaded previously.
//        if let savedImage = persistencyManager.getImage(with: filename) {
//            imageView.image = savedImage
//            return
//        }
//
//        //If the image hasn't already been downloaded, then retrieve it using HTTPClient.
//        DispatchQueue.global().async {
//            let downloadedImage = self.httpClient.downloadImage(coverUrl) ?? UIImage()
//
//            //When the download is complete, display the image in the image view and use the PersistencyManager to save it locally.
//            DispatchQueue.main.async {
//                imageView.image = downloadedImage
//                self.persistencyManager.saveImage(downloadedImage, filename: filename)
//            }
//        }
//    }
    
    
}
