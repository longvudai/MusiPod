//
//  LibraryVC.swift
//  MusiPod
//
//  Created by Dai Long on 7/14/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class LibraryVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let cells = ["Danh sach phat", "Artist", "Album", "Songs", "Downloaded"]
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    
    func setupView() {
        setupNavigationVC()
    }
    
    func setupNavigationVC() {
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension LibraryVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LibraryCell.reuseIdentifier, for: indexPath) as? LibraryCell else {
            fatalError("Unexpected indexPath")
        }
        var i = 0
        cell.titleLabel.text = cells[i]
        i += 1
        return cell
    }
}
