//
//  SearchVC+URLSessionDelegate.swift
//  MusiPod
//
//  Created by Dai Long on 7/13/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

extension SearchVC: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL) {
        guard let sourceURL = downloadTask.currentRequest?.url, let originURL = downloadTask.originalRequest?.url else {return}
        
        let downloadModel = downloadService.activedDownload[originURL]
        downloadService.activedDownload[originURL] = nil

        let destinationURL = save2doc(for: sourceURL)
        print(destinationURL)
        
        let fileManager = FileManager.default
        
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
            downloadModel?.track.downloaded = true

        }
        catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        
        if let index = downloadModel?.track.index {
            DispatchQueue.main.async {
                self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            }
        }
    }
    
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        
        guard let url = downloadTask.originalRequest?.url,
            let download = downloadService.activedDownload[url]  else { return }
        
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        
        DispatchQueue.main.async {
            if let trackCell = self.tableView.cellForRow(at: IndexPath(row: download.track.index,
                                                                       section: 0)) as? TrackCell {
                trackCell.updateDisplay(progress: download.progress)
            }
        }
    }
}
