//
//  SearchVC.swift
//  MusiPod
//
//  Created by Dai Long on 6/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit


class SearchVC: UIViewController, TrackSubcriber {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var miniPlayerView: UIView!
    
    // MARK: - Properties
    var miniPlayer: MiniPlayerVC?
    let searchVC = UISearchController(searchResultsController: nil)
    
    // MARK: - Service
    
    let downloadService = DownloadService()
    let queryService = QueryService()
    var allLocalTracks: [Track] = []
    var searchOnlineResult: [Track] = []
    
    lazy var downloadSession: URLSession = {
        let configure = URLSessionConfiguration.background(withIdentifier: "bgSessionConfig")
        return URLSession(configuration: configure,
                          delegate: self,
                          delegateQueue: nil)
    }()
    
    // MARK: -
    
    var crtTrack: Track?
    
    // MARK: -
    
    var documentURL = FileManager.default.urls(for: .documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask)[0]
    
    private let estimatedRowHeight = CGFloat(44.0)
    
    var hasTrack: Bool = false
    
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()

        downloadService.downloadSession = downloadSession
        downloadService.delegate = self
    }
    
    deinit {
        print("Main View controller was deinited")
    }
    
    
    // MARK: - Methods
    
    fileprivate func setupView() {
        setupNavigationController()
        setupTableView()
        setupMiniPlayer()
        setupMessageLabel()
    }

    func updateView() {
        tableView.isHidden = !hasTrack
        messageLabel.isHidden = hasTrack
        miniPlayerView.isHidden = !hasTrack
    }
    
    private func setupMessageLabel() {
        messageLabel.text = "Search what you want."
    }
    
    fileprivate func setupNavigationController() {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchVC
        
        searchVC.searchBar.delegate = self
        
        searchVC.isActive = true
        searchVC.obscuresBackgroundDuringPresentation = false
        searchVC.searchBar.placeholder = "Enter songs name or artist"
        definesPresentationContext = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? MiniPlayerVC {
            miniPlayer = destination
            miniPlayer?.delegate = self
        }
    }
    
    func save2doc(for url: URL) -> URL {
        return documentURL.appendingPathComponent(url.lastPathComponent)
    }
}

// MARK: - Table View

extension SearchVC: UITableViewDataSource, UITableViewDelegate {
    private func setupTableView() {
        tableView.isHidden = !hasTrack
        tableView.estimatedRowHeight = estimatedRowHeight
        
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchOnlineResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Create a cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TrackCell.reuseIdentifier, for: indexPath) as? TrackCell else {
                fatalError("Unexpected Index Path")
                }
        
        cell.delegate = self
        
        let track = searchOnlineResult[indexPath.row]
        // Configure Cell
        
        configure(cell,
                  for: track,
                  with: downloadService.activedDownload[track.UrlJunDownload])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = searchOnlineResult[indexPath.row]
        crtTrack = item
        miniPlayer?.configure(crtTrack)
        miniPlayer?.avplayerAPI.player?.play()
        
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    // Configure Cell
    
    func configure(_ cell: TrackCell, for track: Track, with download: Download?) {
        cell.titleLabel.text = track.Title
        cell.artistLabel.text = track.Artist
        
        // Show/hide download controls Pause/Resume, Cancel buttons, progress info
        var showDownloadControls = false
        
        // Non-nil Download object means a download is in progress
        if let download = download {
            showDownloadControls = true
            let image = download.isDownloading ? #imageLiteral(resourceName: "pause-dl-30") : #imageLiteral(resourceName: "synchronize-30")
            cell.pauseButton.setImage(image, for: .normal)
        }
        
        cell.pauseButton.isHidden = !showDownloadControls
        cell.cancelButton.isHidden = !showDownloadControls
        cell.progressView.isHidden = !showDownloadControls        
        cell.downloadButton.isHidden = track.downloaded || showDownloadControls
        
  //      print("track downloaded is: \(track.downloaded)")
  //      print(track)
    }

}

// MARK: - MiniPlayer Delegate

extension SearchVC: MiniPlayerDelegate {
    
    fileprivate func setupMiniPlayer() {
        miniPlayerView.isHidden = true
    }
    
    func expand(_ track: Track) {

        guard let maxiPlayer = storyboard?.instantiateViewController(withIdentifier: "MaxiPlayerVC")
            as? MaxiPlayerVC else {
                assertionFailure("No view controller ID MaxiPlayer in storyboard")
                return
        }
        
        maxiPlayer.crtTrack = crtTrack

        present(maxiPlayer, animated: true)
    }
}


// MARK: - Track Cell Delegate

extension SearchVC: TrackCellDelegate {
    func pauseTapped(_ cell: TrackCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let track = searchOnlineResult[indexPath.row]
            
            downloadService.pauseDownload(track)
            reload(indexPath.row)
        }
    }
    
    func resumeTapped(_ cell: TrackCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let track = searchOnlineResult[indexPath.row]
            downloadService.resumeDownload(track)
            reload(indexPath.row)
        }
    }
    
    func cancelTapped(_ cell: TrackCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let track = searchOnlineResult[indexPath.row]
            downloadService.cancelDownload(track)
            reload(indexPath.row)
        }
    }
    
    func downloadTapped(_ cell: TrackCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let track = searchOnlineResult[indexPath.row]
            downloadService.startDownload(track)
            reload(indexPath.row)
        }
    }
    
    fileprivate func reload(_ row: Int) {
        tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
    }
    
}

extension SearchVC: URLSessionDelegate {
    
    // Standard background session handler
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                let completionHandler = appDelegate.backgroundSessionCompletionHandler {
                appDelegate.backgroundSessionCompletionHandler = nil
                completionHandler()
            }
        }
    }
    
}

extension SearchVC: DownloadServiceDelegate {
    func trackAlreadyDownloaded() {
        let toastMessage = ToastMessage()
        let heightView = view.frame.height
        let widthView = view.frame.width
        toastMessage.frame = CGRect(x: 0,
                                    y: heightView - 150,
                                    width: widthView - 50,
                                    height: 20)
        
        toastMessage.text = "You already downloaded this track"
        toastMessage.textAlignment = .center
        toastMessage.sizeToFit()
        toastMessage.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastMessage.numberOfLines = 0
        toastMessage.alpha = 1.0
        toastMessage.layer.cornerRadius = 5
        toastMessage.clipsToBounds = true
        toastMessage.frame.origin.x = (widthView/2) - (toastMessage.frame.width/2)
        self.view.addSubview(toastMessage)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0.3,
                       options: .curveEaseOut,
                       animations: {toastMessage.alpha = 0.0}) { (isCompleted) in toastMessage.removeFromSuperview()}
    }
}

