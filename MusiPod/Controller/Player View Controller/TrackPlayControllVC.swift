//
//  TrackPlayControllVC.swift
//  MusiPod
//
//  Created by Dai Long on 7/8/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer



class TrackPlayControllVC: UIViewController, TrackSubcriber {
    
    // MARK: - Properties
    var avplayerAPI = AVPLayerAPI.shared
    
    private var timeObserverToken: Any?
    var timer: Timer?

    var crtTrack: Track? {
        didSet {
    
        //    print("remove observer")
        }
    }
    
    let commandCenter = MPRemoteCommandCenter.shared()
    
    
    // MARK: - IBOutlets
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var rewindBtn: UIButton!
    @IBOutlet weak var fastForwardBtn: UIButton!
    
    @IBOutlet weak var scrubberSlider: UISlider!

    @IBOutlet weak var volumeSlider: UISlider!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    
    @IBOutlet weak var countdownTimingLabel: UILabel!
    @IBOutlet weak var countTimingLabel: UILabel!
    
    // MARK: - View life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        configure(crtTrack)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fastForwardBtn.addTarget(self, action: #selector(fastForwardBtnDown), for: .touchDown)
        fastForwardBtn.addTarget(self, action: #selector(buttonUp), for: [.touchUpInside, .touchUpOutside])
        
        rewindBtn.addTarget(self, action: #selector(rewindBtnDown), for: .touchDown)
        rewindBtn.addTarget(self, action: #selector(buttonUp), for: [.touchUpInside, .touchUpOutside])

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        avplayerAPI.player?.removeObserver(self, forKeyPath: #keyPath(AVPlayer.timeControlStatus))
        if let timeObserverToken = timeObserverToken {
            avplayerAPI.player?.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }

    deinit {
        print("TrackPlayerControl View controller was denited")
    }

    // MARK: - IBActions
    @IBAction func playAction (_ sender: UIButton) {
        guard crtTrack != nil else {
            print("Can't play track!")
            return
        }
        avplayerAPI.play()
    }
    
    @objc func fastForwardBtnDown(_ sender: UIButton) {
        timer = Timer.scheduledTimer(timeInterval: 0.3,
                                     target: self,
                                     selector: #selector(fastForwardButtonWasPressed),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    @objc func rewindBtnDown(_ sender: UIButton) {
        timer = Timer.scheduledTimer(timeInterval: 0.3,
                                     target: self,
                                     selector: #selector(rewindButtonWasPressed),
                                     userInfo: nil,
                                     repeats: true)
    }

    @objc func buttonUp(_ sender: UIButton) {
        timer?.invalidate()
        avplayerAPI.rate = 1.0
    }
    
    
    @objc func rewindButtonWasPressed(_ sender: UIButton) {
        guard let player = avplayerAPI.player else {return}

        // Rewind no faster than -2.0.
        
        avplayerAPI.rate = max(player.rate - 2.0, -2.0)
    }

    @objc func fastForwardButtonWasPressed(_ sender: UIButton) {
        guard let player = avplayerAPI.player else {return}

        // Fast forward no faster than 2.0.
        
        avplayerAPI.rate = min(player.rate + 2.0, 2.0)
    }
    
    @IBAction func scrubberSliderValueChanged (_ sender: UISlider) {
        let seconds : Int64 = Int64(scrubberSlider.value)
        let targetTime:CMTime = CMTimeMake(seconds, 1)
        
        avplayerAPI.player!.seek(to: targetTime)
        
        if avplayerAPI.player!.rate == 0 {
            avplayerAPI.player?.play()
        }
        
    }

    @IBAction func adjustVolume(_ sender: UISlider) {
        avplayerAPI.player?.volume = volumeSlider.value
    }
    
    
    func getFormatedTime(FromTime timeDuration:Float) -> String {
        
        let minutes = Int(timeDuration) / 60 % 60
        let seconds = Int(timeDuration) % 60
        let strDuration = String(format:"%02d:%02d", minutes, seconds)
        return strDuration
    }
    
    func configure(_ track: Track?) {
        if let track = track {
            titleLabel.text = track.Title
            artistLabel.text = track.Artist
            
            let durationTime = avplayerAPI.playerItem?.asset.duration
            let seconds: Float64 = CMTimeGetSeconds(durationTime!)
            
            scrubberSlider.minimumValue = 0
            scrubberSlider.maximumValue = Float(seconds)
            scrubberSlider.isContinuous = true
            scrubberSlider.addTarget(self, action: #selector(scrubberSliderValueChanged(_:)), for: UIControlEvents.touchUpInside)
            scrubberSlider.value = avplayerAPI.currentTime
            
            volumeSlider.minimumValue = 0.0
            volumeSlider.maximumValue = 1.0
            volumeSlider.value = avplayerAPI.player?.volume ?? 0
            
            
            countTimingLabel.text = getFormatedTime(FromTime: avplayerAPI.currentTime)
            countdownTimingLabel.text = getFormatedTime(FromTime: avplayerAPI.duration - avplayerAPI.currentTime)
            
            avplayerAPI.player?.addObserver(self,
                                            forKeyPath: #keyPath(AVPlayer.timeControlStatus),
                                            options: [.initial,.new],
                                            context: nil)
            //Add Periodic TimeObserver to Update Music Playback Slider
            timeObserverToken = avplayerAPI.player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main) { [unowned self] (CMTime) in
                if self.avplayerAPI.player?.currentItem?.status == .readyToPlay {
                    let time: Float64 = CMTimeGetSeconds((self.avplayerAPI.player?.currentTime())!)
                    self.scrubberSlider.value = Float(time)
                }

                if let crtTime = self.avplayerAPI.playerItem?.currentTime(),
                    let durationTime = self.avplayerAPI.playerItem?.asset.duration {
                    self.countTimingLabel.text = self.getFormatedTime(FromTime: Float( CMTimeGetSeconds(crtTime) ) )
                    let recordingDuration = Float(CMTimeGetSeconds(durationTime)) - Float(CMTimeGetSeconds(crtTime))
                    self.countdownTimingLabel.text = self.getFormatedTime(FromTime: recordingDuration)
                }


            }
        }
            
        else {
            titleLabel.text = nil
            artistLabel.text = nil
        }
    }
   
}

extension TrackPlayControllVC {
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        
        if keyPath == #keyPath(AVPlayer.timeControlStatus) {
            
            let status: AVPlayerTimeControlStatus
            
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerTimeControlStatus(rawValue: statusNumber.intValue)!
            }
            else {
                status = .waitingToPlayAtSpecifiedRate
            }
            
            switch status {
            case .paused:
           //     print("pause on playcontrol")
                playBtn.setImage(#imageLiteral(resourceName: "play-filled-50"), for: .normal)
            case .playing:
            //    print("play on playcontrol")
                playBtn.setImage(#imageLiteral(resourceName: "pause-filled-50"), for: .normal)
            case .waitingToPlayAtSpecifiedRate:
           //     print("wait on playcontrol")
                playBtn.setImage(#imageLiteral(resourceName: "play-filled-50"), for: .normal)
            }
        }
    }
}
