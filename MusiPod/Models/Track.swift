//
//  Track.swift
//  MusiPod
//
//  Created by Dai Long on 7/4/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

class Track: Codable {
    let Title: String
    let Artist: String
    let Avatar: URL
    var UrlJunDownload: URL
    let LyricsUrl: URL
    //   private var UrlSource: String
    //   private var SiteId: String
    let HostName: String
    let index: Int
    var downloaded = false
    
    
    var localURL: URL?
    
    init(Title: String,
         Artist: String,
         UrlJunDownload: URL,
         HostName: String,
         Avatar: URL,
         LyricsUrl: URL,
         index: Int) throws {
        self.Title = Title
        self.Artist = Artist
        self.Avatar = Avatar
        self.UrlJunDownload = UrlJunDownload
        self.LyricsUrl = LyricsUrl
        self.HostName = HostName
        self.index = index
    }
}
