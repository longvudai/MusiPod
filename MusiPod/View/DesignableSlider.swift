//
//  DesignableSlider.swift
//  MusiPod
//
//  Created by Dai Long on 7/6/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
@IBDesignable
class DesignableSlider: UISlider {
    @IBInspectable var thumbImage: UIImage? {
        didSet {
            setThumbImage(thumbImage, for: .normal)
        }
    }
    
    @IBInspectable var thumbHighlightedImage: UIImage? {
        didSet {
            setThumbImage(thumbHighlightedImage, for: .highlighted)
        }
    }
}
