//
//  ToastMessage.swift
//  MusiPod
//
//  Created by Dai Long on 7/14/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class ToastMessage: UILabel {
    let padding = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let superSizeThatFit = super.sizeThatFits(size)
        
        let width = superSizeThatFit.width + padding.left + padding.right
        let height = superSizeThatFit.height + padding.top + padding.bottom
        
        return CGSize(width: width, height: height)
    }
}
