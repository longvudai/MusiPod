//
//  TrackCell.swift
//  MusiPod
//
//  Created by Dai Long on 7/4/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

protocol TrackCellDelegate {
    func pauseTapped(_ cell: TrackCell)
    func resumeTapped(_ cell: TrackCell)
    func cancelTapped(_ cell: TrackCell)
    func downloadTapped(_ cell: TrackCell)
}

class TrackCell: UITableViewCell {
    
    static let reuseIdentifier = "TrackCell"
    
    var delegate: TrackCellDelegate?
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    // MARK: -
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateDisplay(progress: Float) {
        progressView.progress = progress
    }
    
    
    @IBAction func cancelwasPressed(_ sender: Any) {
        delegate?.cancelTapped(self)
    }
    
    @IBAction func resumeWasPressed(_ sender: Any) {
        if pauseButton.currentImage == #imageLiteral(resourceName: "pause-dl-30") {
            delegate?.pauseTapped(self)
        }
        else {
            delegate?.resumeTapped(self)
        }
    }
    
    @IBAction func downloadwasPressed(_ sender: Any) {
        delegate?.downloadTapped(self)
    }
    
}
